# AKI Examples

## [1. helloworld](https://gitee.com/openharmony-sig/aki/tree/master/example/ohos/1_helloworld)

Example of using AKI hello world

## [2. applicationInfo](https://gitee.com/openharmony-sig/aki/tree/master/example/ohos/2_applicationInfo)

## [3. callback](https://gitee.com/openharmony-sig/aki/tree/master/example/ohos/3_callback)

Example of using the AKI callback

## [4. hybrid_napi](https://gitee.com/openharmony-sig/aki/tree/master/example/ohos/4_hybrid_napi)

Example of using AKI with Node-API

## [5. bind_from_js](https://gitee.com/openharmony-sig/aki/tree/master/example/ohos/5_bind_from_js)

Example of using AKI to bind a JS function (not a callback) and call it from C++

## [6. structure](https://gitee.com/openharmony-sig/aki/tree/master/example/ohos/6_structure)

Example of using the AKI structure object

## [7. array_buffer_to_native](https://gitee.com/openharmony-sig/aki/tree/master/example/ohos/7_array_buffer_to_native)

Example of using the AKI ArrayBuffer

## [8. async_tasks](https://gitee.com/openharmony-sig/aki/tree/master/example/ohos/8_async_tasks)

Example of using AKI to bind a C++ function and call it asynchronously from JS
## [9. enumeration](https://gitee.com/openharmony-sig/aki/tree/master/example/ohos/9_enumeration)

Example of using AKI enumeration

## 10. map\_for\_object

Example of using C++ **std::map<std::string, T>** to map JS **Object**
## 11. napi_value

Example of using C++ **napi_value** to map OpenHarmony data types
## 12. rawfilejs

Example of using the AKI rawfile. For details, see [Rawfile Guide](https://docs.openharmony.cn/pages/v4.0/en/application-dev/napi/rawfile-guidelines.md/).

## [17.worker](17_worker)

Example of using AKI in worker threads

## 18. napi\_call\_function_exception

Example of capturing napi\_call\_function invocation exceptions in AKI

## 19. class_extend

Example of binding a class and its inheritance relationship
