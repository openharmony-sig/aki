# the minimum version of CMake.
cmake_minimum_required(VERSION 3.5.0)
project(aki_performance_test)

set(CMAKE_CXX_STANDARD 17)

set(NATIVERENDER_ROOT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../../../../../../../)

if(DEFINED PACKAGE_FIND_FILE)
    include(${PACKAGE_FIND_FILE})
endif()

include_directories(${NATIVERENDER_ROOT_PATH}
                    ${NATIVERENDER_ROOT_PATH}/include
                    ${CMAKE_CURRENT_SOURCE_DIR}/)

file(GLOB_RECURSE SOURCES "testManager/*.cpp" "testResources/*.cpp")

add_library(entry SHARED napi_init.cpp ${SOURCES})

target_link_libraries(entry PUBLIC libace_napi.z.so libhilog_ndk.z.so)

add_subdirectory(${NATIVERENDER_ROOT_PATH} aki)
target_link_libraries(entry PUBLIC aki_jsbind)