/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AKI_VALUE_CMP_TESTTIME_H
#define AKI_VALUE_CMP_TESTTIME_H

#include <chrono>
#include <string>
#include <cstdint>
#include "testManager/aki_test_manager.h"

namespace AKITEST {

class TestTime {
public:
    TestTime() = default;
    ~TestTime() = default;
    void MarkStart();
    void MarkEnd();
    int64_t CalculateAverageTime(int testCount);
    void DataReset();
    template <typename Ret, typename... Args>
    void NapiTestTime(int testCount, std::string moudlueName, std::string apiName, Ret (*func)(Args...), Args... args)
    {
        MarkStart();
        for (int index = 0; index < testCount; index++) {
            func(std::forward<Args>(args)...);
        }
        MarkEnd();
        int averageTime = CalculateAverageTime(testCount);
        AKITEST::AkiTestManager::GetInstance().UpdateTestTime(moudlueName.c_str(), apiName.c_str(), averageTime, 0);
        DataReset();
    }
    template <typename Ret, typename... Args>
    void AkiTestTime(int testCount, std::string moudlueName, std::string apiName, Ret (*func)(Args...),
                     Args &&...args)
    {
        MarkStart();
        for (int index = 0; index < testCount; index++) {
            func(std::forward<Args>(args)...);
        }
        MarkEnd();
        int averageTime = CalculateAverageTime(testCount);
        AKITEST::AkiTestManager::GetInstance().UpdateTestTime(moudlueName.c_str(), apiName.c_str(), 0, averageTime);
        DataReset();
    }

private:
    int count_{0};
    int64_t differenceSum_{0};
    int64_t averageNum_{0};
    std::chrono::nanoseconds startTime_;
    std::chrono::nanoseconds endTime_;
};

}

#endif //AKI_VALUE_CMP_TESTTIME_H
