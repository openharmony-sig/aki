/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AKI_VALUE_CMP_AKI_TEST_MANAGER_H
#define AKI_VALUE_CMP_AKI_TEST_MANAGER_H

#include <map>
#include <cstring>
#include "napi/native_api.h"

namespace AKITEST {

struct TestTimeInfo {
    int testTimeNapi = 0;
    int testTimeAki = 0;
};

using NoParamNoReturnFunc = void (*)();

class AkiTestManager {
public:
    AkiTestManager() = default;
    ~AkiTestManager() = default;
    static AkiTestManager &GetInstance();
    void AddTestFunction(const std::string moudle, NoParamNoReturnFunc func);
    void Start();
    napi_env GetNapiEnv();
    void SetNapiEnv(napi_env env);
    void UpdateTestTime(std::string moduleName, std::string api, int napiTestTime, int akiTestTime);
    void PrintTestTime();

private:
    napi_env napiEnv_;
    static AkiTestManager akiTestManager_;
    std::map<std::string, NoParamNoReturnFunc> functionMap_;
    std::map<std::string, std::map<std::string, TestTimeInfo>> testTimeMap_;
};
} // namespace AKITEST

#endif //AKI_VALUE_CMP_AKI_TEST_MANAGER_H
