/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "testManager/aki_test_time.h"

namespace AKITEST {

void TestTime::MarkStart()
{
    auto now = std::chrono::high_resolution_clock::now();
    startTime_ = std::chrono::time_point_cast<std::chrono::nanoseconds>(now).time_since_epoch();
}

void TestTime::MarkEnd()
{
    auto now = std::chrono::high_resolution_clock::now();
    endTime_ = std::chrono::time_point_cast<std::chrono::nanoseconds>(now).time_since_epoch();
}

int64_t TestTime::CalculateAverageTime(int testCount)
{
    differenceSum_ = (endTime_.count() - startTime_.count());
    if (testCount != 0) {
        averageNum_ = differenceSum_ / testCount;
    } else {
        averageNum_ = 0;
    }
    count_ = testCount;
    return averageNum_;
}

void TestTime::DataReset()
{
    count_ = 0;
    averageNum_ = 0;
    differenceSum_ = 0;
}

}
