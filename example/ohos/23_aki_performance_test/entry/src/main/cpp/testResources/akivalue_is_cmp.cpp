/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstring>
#include "hilog/log.h"
#include "aki/jsbind.h"
#include "napi/native_api.h"
#include "testManager/aki_test_time.h"
#include "testManager/aki_test_manager.h"

#include "akivalue_is_cmp.h"

namespace AKITEST {
#define TEST_COUNT 100000
#define TEST_NUMBER 42.0
#define ZERO 0
#define ONE 1
#define TWO 2
#define THREE 3
#define FOUR 4
#define FIVE 5
#define SIX 6
#define SEVEN 7
#define EIGHT 8

template <typename R, typename... args>
struct FunctionInfo {
    R (*func)(args...);
    std::string name;
};

using NapiCallback = napi_value (*)(napi_env env, napi_callback_info info);
NapiCallback CallbackTest = [](napi_env env, napi_callback_info info) -> napi_value {
    return nullptr;
};

bool NapiIsUndefinded(napi_env env, napi_value value)
{
    napi_valuetype type;
    if (napi_typeof(env, value, &type) != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiIsUndefinded", "napi_typeof failed");
    }
    return type == napi_undefined;
}

bool NapiIsNull(napi_env env, napi_value value)
{
    napi_valuetype type;
    if (napi_typeof(env, value, &type) != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiIsNull", "napi_typeof failed");
    }
    return type == napi_null;
}

bool NapiIsBool(napi_env env, napi_value value)
{
    napi_valuetype type;
    if (napi_typeof(env, value, &type) != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiIsBool", "napi_typeof failed");
    }
    return type == napi_boolean;
}

bool NapiIsNumber(napi_env env, napi_value value)
{
    napi_valuetype type;
    if (napi_typeof(env, value, &type) != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiIsNumber", "napi_typeof failed");
    }
    return type == napi_number;
}

bool NapiIsString(napi_env env, napi_value value)
{
    napi_valuetype type;
    if (napi_typeof(env, value, &type) != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiIsString", "napi_typeof failed");
    }
    return type == napi_string;
}

bool NapiIsObject(napi_env env, napi_value value)
{
    napi_valuetype type;
    if (napi_typeof(env, value, &type) != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiIsObject", "napi_typeof failed");
    }
    return type == napi_object;
}

bool NapiIsObjectArray(napi_env env, napi_value value)
{
    bool result;
    if (napi_is_array(env, value, &result) != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiIsObjectArray", "napi_is_array failed");
    }
    return result;
}

bool NapiIsFunction(napi_env env, napi_value value)
{
    napi_valuetype type;
    if (napi_typeof(env, value, &type) != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiIsFunction", "napi_typeof failed");
    }
    return type == napi_function;
}

void AkiValueIsCmp::NapiValueIsCheckTest(void)
{
    size_t argc = EIGHT;
    napi_value args[argc];
    napi_env env = AKITEST::AkiTestManager::GetInstance().GetNapiEnv();
    napi_get_null(env, &args[ZERO]);
    napi_get_boolean(env, true, &args[ONE]);
    napi_create_double(env, TEST_NUMBER, &args[TWO]);
    napi_create_string_utf8(env, "Hello Napi", strlen("Hello Napi"), &args[THREE]);
    napi_create_object(env, &args[FOUR]);
    napi_create_array(env, &args[FIVE]);
    napi_create_function(env, nullptr, ZERO, CallbackTest, nullptr, &args[SIX]);
    napi_get_undefined(env, &args[SEVEN]);

    FunctionInfo<bool, napi_env, napi_value> functionInfos[] = {
        {NapiIsNull, "IsNull"},         {NapiIsBool, "IsBool"},
        {NapiIsNumber, "IsNumber"},     {NapiIsString, "IsString"},
        {NapiIsObject, "IsObject"},     {NapiIsObjectArray, "IsObjectArray"},
        {NapiIsFunction, "IsFunction"}, {NapiIsUndefinded, "IsUndefinded"}};

    for (int index = 0; index < sizeof(functionInfos) / sizeof(FunctionInfo<bool, napi_env, napi_value>); index++) {
        std::string functionName = functionInfos[index].name.c_str();
        AKITEST::TestTime().NapiTestTime(TEST_COUNT,
            "Value", functionName, functionInfos[index].func, env, args[index]);
    }
}

bool AkiIsNull(aki::Value &value)
{
    return value.IsNull();
}

bool AkiIsBool(aki::Value &value)
{
    return value.IsBool();
}

bool AkiIsNumber(aki::Value &value)
{
    return value.IsNumber();
}

bool AkiIsString(aki::Value &value)
{
    return value.IsString();
}

bool AkiIsObject(aki::Value &value)
{
    return value.IsObject();
}

bool AkiIsObjectArray(aki::Value &value)
{
    return value.IsArray();
}

bool AkiIsFunction(aki::Value &value)
{
    return value.IsFunction();
}

bool AkiIsUndefinded(aki::Value &value)
{
    return value.IsUndefined();
}

void AkiValueIsCmp::AkiValueIsCheckTest(void)
{
    size_t argc = EIGHT;
    napi_value args[argc];
    napi_env env = AKITEST::AkiTestManager::GetInstance().GetNapiEnv();
    napi_get_null(env, &args[ZERO]);
    napi_get_boolean(env, true, &args[ONE]);
    napi_create_double(env, TEST_NUMBER, &args[TWO]);
    napi_create_string_utf8(env, "Hello Napi", strlen("Hello Napi"), &args[THREE]);
    napi_create_object(env, &args[FOUR]);
    napi_create_array(env, &args[FIVE]);
    napi_create_function(env, nullptr, ZERO, CallbackTest, nullptr, &args[SIX]);
    napi_get_undefined(env, &args[SEVEN]);
    aki::Value akiargs[EIGHT];
    for (int index = ZERO; index < argc; index++) {
        akiargs[index] = args[index];
    }

    FunctionInfo<bool, aki::Value &> functionInfos[] = {
        {AkiIsNull, "IsNull"},         {AkiIsBool, "IsBool"},
        {AkiIsNumber, "IsNumber"},     {AkiIsString, "IsString"},
        {AkiIsObject, "IsObject"},     {AkiIsObjectArray, "IsObjectArray"},
        {AkiIsFunction, "IsFunction"}, {AkiIsUndefinded, "IsUndefinded"}};
    for (int index = 0; index < sizeof(functionInfos) / sizeof(FunctionInfo<bool, aki::Value &>); index++) {
        std::string functionName = functionInfos[index].name.c_str();
        AKITEST::TestTime().AkiTestTime(TEST_COUNT, "Value", functionName, functionInfos[index].func, akiargs[index]);
    }
}
}