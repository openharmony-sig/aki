/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AKIVALUE_CALLMETHOD_CMP_H
#define AKIVALUE_CALLMETHOD_CMP_H

#include "napi/native_api.h"
#include "aki/jsbind.h"

namespace AKITEST {
class AkiValueCallMethod {
public:
    static void AkiCallMethodTest(void);
    static void NapiCallMethodTest(void);
};
} // namespace AKITEST

#endif //AKIVALUE_CALLMETHOD_CMP_H
