/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "aki/jsbind.h"
#include "hilog/log.h"
#include "aki_arraybuffer_cmp.h"
#include "testManager/aki_test_time.h"
#include "testManager/aki_test_manager.h"

#define TEST_COUNT 100000
#define ZERO 0
#define TEN 10

namespace AKITEST {
void NapiCreateArrayBuffer(napi_env env)
{
    napi_status status;
    size_t byteLength = TEN;
    napi_value arrayBuffer = nullptr;
    uint8_t *data = nullptr;
    status = napi_create_arraybuffer(env, byteLength, (void **)&data, &arrayBuffer);
    for (int i = ZERO; i < byteLength; i++) {
        data[i] = i;
    }
    if (status != napi_ok) {
        OH_LOG_Print(LOG_APP, LOG_ERROR, LOG_DOMAIN, "NapiCreateArrayBuffer", "constructor failed");
    }
}

void AkiArrayBufferCmp::NapiArrayBufferTest(void)
{
    napi_env env = AkiTestManager::GetInstance().GetNapiEnv();
    AKITEST::TestTime().NapiTestTime(TEST_COUNT, "ArrayBuffer", "constructor", NapiCreateArrayBuffer, env);
}

void AkiCreateArrayBuffer()
{
    size_t byteLength = TEN;
    uint8_t data[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    aki::ArrayBuffer arrayBuffer(data, byteLength, aki::ArrayBuffer::Typed::BUFF);
}

void AkiArrayBufferCmp::AkiArrayBufferTest(void)
{
    AKITEST::TestTime().AkiTestTime(TEST_COUNT, "ArrayBuffer", "constructor", AkiCreateArrayBuffer);
}
}
