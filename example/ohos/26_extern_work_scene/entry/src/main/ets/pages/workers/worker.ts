/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import worker, { MessageEvents } from '@ohos.worker';
import libworker from 'libentry.so'

const workerPort = worker.workerPort

workerPort.onmessage = (e: MessageEvents): void => {
  let info = e.data
  if (info == "start") {
    let test = new libworker.Message("hello")
    libworker.AsyncTaskMessageReturnMessage(test, "two").then((result) => {
      console.log("worker thread " + result.message)
    })
  }else if(info == 'close') {
    libworker.JSBind.unbindFunction("WorkTest");
    workerPort.close()
  } else {
    console.log("worker thread callJsFunc " + libworker.CallJsFunc("WorkTest"))
    console.log("worker thread callJsFunc " + libworker.CallJsFunc("MainTest"))
  }
}

workerPort.onerror = () => {
  console.log("worker thread onerror")
}
libworker.JSBind.bindFunction("WorkTest", WorkTest);
function WorkTest() {
  console.log("worker test_worker");
  return "test_worker";
}
